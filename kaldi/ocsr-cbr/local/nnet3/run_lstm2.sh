#!/bin/bash

#    This is the standard "lstm" system, built in nnet3; this script
# is the version that's meant to run with data-cleanup, that doesn't
# support parallel alignments.


# by default:
# local/nnet3/run_lstm.sh

set -e -o pipefail -u

# First the options that are passed through to run_ivector_common.sh
# (some of which are also used in this script directly).
nj=1
decode_nj=1
train_set=train
gmm=tri3b  # this is the source gmm-dir for the data-type of interest; it
                  # should have alignments for the specified training data.
num_threads=1
nnet3_affix=  # cleanup affix for exp dirs, e.g. _cleaned

# Options which are not passed through to run_ivector_common.sh
affix=
common_egs_dir=

# LSTM options
train_stage=-10
label_delay=0
cell_dim=512 #alt
hidden_dim=512 #alt
recurrent_projection_dim=64 #alt
non_recurrent_projection_dim=64 #alt
chunk_width=12 #alt
chunk_left_context=24 #alt
chunk_right_context=24 #alt

# training options
srand=0
num_epochs=3 #alt
initial_effective_lrate=0.0123 #alt
final_effective_lrate=0.00123 #alt
momentum=0.3 #alt 
num_chunk_per_minibatch=60 #alt
samples_per_iter=12000 #alt
remove_egs=true

#decode options
extra_left_context=30 #alt
extra_right_context=30 #alt
frames_per_chunk=

. ./cmd.sh
. ./path.sh
. ./utils/parse_options.sh

gmm_dir=exp/${gmm}
graph_dir=$gmm_dir/graph
ali_dir=exp/${gmm}_ali_${train_set}_sp_comb
dir=exp/nnet3${nnet3_affix}/lstm${affix:+_$affix}
if [ $label_delay -gt 0 ]; 
    then dir=${dir}_ld$label_delay; 
fi
dir=${dir}_sp
train_data_dir=data/${train_set}_sp_hires_comb
train_ivector_dir=exp/nnet3${nnet3_affix}/ivectors_${train_set}_sp_hires_comb

for f in $train_data_dir/feats.scp $train_ivector_dir/ivector_online.scp \
    $graph_dir/HCLG.fst $ali_dir/ali.1.gz $gmm_dir/final.mdl; do
    [ ! -f $f ] && echo "$0: expected file $f to exist" && exit 1
done

echo "$0: creating neural net configs using the xconfig parser";

# configs old
#input dim=1024 name=ivector
#input dim=1024 name=input
#fixed-affine-layer name=lda input=Append(-2,-1,0,1,2,ReplaceIndex(ivector, t, 0)) affine-transform-file=foo/bar/lda.mat
#lstm-layer name=lstm cell-dim=1024
#output-layer name=output dim=1924 input=Append(-1,0,1)

num_targets=$(tree-info $ali_dir/tree | grep num-pdfs | awk '{print $2}')
[ -z $num_targets ] && { echo "$0: error getting num-targets"; exit 1; }

lstm_opts="decay-time=12 cell-dim=$cell_dim" #alt
lstm_opts+=" recurrent-projection-dim=$recurrent_projection_dim"
lstm_opts+=" non-recurrent-projection-dim=$non_recurrent_projection_dim"

rm -rf $dir/configs

mkdir -p $dir/configs
cat <<EOF > $dir/configs/network.xconfig #alt
input dim=100 name=ivector
input dim=40 name=input

# please note that it is important to have input layer with the name=input
# as the layer immediately preceding the fixed-affine-layer to enable
# the use of short notation for the descriptor
fixed-affine-layer name=lda delay=$label_delay input=Append(-2,-1,0,1,2,ReplaceIndex(ivector, t, 0)) affine-transform-file=$dir/configs/lda.mat

# check steps/libs/nnet3/xconfig/lstm.py for the other options and defaults
fast-lstmp-layer name=lstm1-forward input=lda delay=-1 $lstm_opts
fast-lstmp-layer name=lstm1-backward input=lda delay=1 $lstm_opts

fast-lstmp-layer name=lstm2-forward input=Append(lstm1-forward, lstm1-backward) delay=-2 $lstm_opts
fast-lstmp-layer name=lstm2-backward input=Append(lstm1-forward, lstm1-backward) delay=2 $lstm_opts

output-layer name=output output-delay=$label_delay dim=$num_targets max-change=0.9
EOF
steps/nnet3/xconfig_to_configs.py --xconfig-file $dir/configs/network.xconfig --config-dir $dir/configs || exit 1

if [[ $(hostname -f) == *.clsp.jhu.edu ]] && [ ! -d $dir/egs/storage ]; then
    utils/create_split_dir.pl /export/b0{3,4,5,6}/$USER/kaldi-data/egs/ocsr-cbr-$(date +'%m_%d_%H_%M')/s5_r2/$dir/egs/storage $dir/egs/storage
fi

steps/nnet3/train_rnn.py --stage=$train_stage \
    --cmd="$decode_cmd" \
    --feat.online-ivector-dir=$train_ivector_dir \
    --feat.cmvn-opts="--norm-means=false --norm-vars=false" \
    --trainer.srand=$srand \
    --trainer.num-epochs=$num_epochs \
    --trainer.samples-per-iter=$samples_per_iter \
    --trainer.optimization.num-jobs-final=1 \
    --trainer.optimization.initial-effective-lrate=$initial_effective_lrate \
    --trainer.optimization.final-effective-lrate=$final_effective_lrate \
    --trainer.optimization.shrink-value 0.594 \
    --trainer.rnn.num-chunk-per-minibatch=$num_chunk_per_minibatch \
    --trainer.optimization.momentum=$momentum \
    --egs.opts " --nj 3 " \
    --egs.chunk-width=$chunk_width \
    --egs.chunk-left-context=$chunk_left_context \
    --egs.chunk-right-context=$chunk_right_context \
    --egs.chunk-left-context-initial=0 \
    --egs.chunk-right-context-final=0 \
    --egs.dir="$common_egs_dir" \
    --cleanup.remove-egs=$remove_egs \
    --cleanup.preserve-model-interval=60 \
    --use-gpu=no \
    --feat-dir=$train_data_dir \
    --ali-dir=$ali_dir \
    --lang=data/lang \
    --dir=$dir  || exit 1; #alt

[ -z $extra_left_context ] && extra_left_context=$chunk_left_context;
[ -z $extra_right_context ] && extra_right_context=$chunk_right_context;
[ -z $frames_per_chunk ] && frames_per_chunk=$chunk_width;
rm $dir/.error 2>/dev/null || true
(
    steps/nnet3/decode.sh --nj $nj --cmd "$decode_cmd"  --num-threads $num_threads \
        --extra-left-context $extra_left_context \
        --extra-right-context $extra_right_context \
        --online-ivector-dir exp/nnet3${nnet3_affix}/ivectors_test_hires \
        ${graph_dir} data/test_hires ${dir}/decode_test || exit 1
) || touch $dir/.error &
wait
[ -f $dir/.error ] && echo "$0: there was a problem while decoding" && exit 1

exit 0;
