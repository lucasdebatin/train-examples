#!/bin/bash

#    This is the standard "tdnn" system, built in nnet3

# by default:
# local/nnet3/run_tdnn.sh

set -e -o pipefail -u

# First the options that are passed through to run_ivector_common.sh
# (some of which are also used in this script directly).
nj=1
decode_nj=1
train_set=train
gmm=tri3b  # this is the source gmm-dir for the data-type of interest; it
                  # should have alignments for the specified training data.
num_threads=1
nnet3_affix=  # cleanup affix for exp dirs, e.g. _cleaned
tdnn_affix=  #affix for TDNN directory e.g. "a" or "b", in case we change the configuration.
common_egs_dir=
egs_stage=0

# Options which are not passed through to run_ivector_common.sh
train_stage=-10
remove_egs=true
relu_dim=1050 #alt

. ./cmd.sh
. ./path.sh
. ./utils/parse_options.sh

# Some of these destinations are created by run_ivector_common.sh

gmm_dir=exp/${gmm}
graph_dir=$gmm_dir/graph
ali_dir=exp/${gmm}_ali_${train_set}_sp_comb
dir=exp/nnet3${nnet3_affix}/tdnn${tdnn_affix}_sp
train_data_dir=data/${train_set}_sp_hires_comb
train_ivector_dir=exp/nnet3${nnet3_affix}/ivectors_${train_set}_sp_hires_comb

for f in $train_data_dir/feats.scp $train_ivector_dir/ivector_online.scp \
    $graph_dir/HCLG.fst $ali_dir/ali.1.gz $gmm_dir/final.mdl; do
    [ ! -f $f ] && echo "$0: expected file $f to exist" && exit 1
done

echo "$0: creating neural net configs using the xconfig parser";
# configs old
# input dim=1024 name=ivector
# input dim=1024 name=input
# fixed-affine-layer name=lda input=Append(-2,-1,0,1,2,ReplaceIndex(ivector, t, 0)) affine-transform-file=foo/bar/lda.mat
# relu-renorm-layer name=tdnn1 dim=512 input=Append(0, IfDefined(-1))
# relu-renorm-layer name=tdnn2 dim=512 input=Append(0, IfDefined(-2))
# relu-renorm-layer name=tdnn3 dim=512 input=Append(0, IfDefined(-2))
# output-layer name=output include-log-softmax=false dim=1924

num_targets=$(tree-info $ali_dir/tree | grep num-pdfs | awk '{print $2}')

rm -rf $dir/configs

mkdir -p $dir/configs
cat <<EOF > $dir/configs/network.xconfig #alt
input dim=100 name=ivector
input dim=40 name=input

# please note that it is important to have input layer with the name=input
# as the layer immediately preceding the fixed-affine-layer to enable
# the use of short notation for the descriptor
fixed-affine-layer name=lda input=Append(-2,-1,0,1,2,ReplaceIndex(ivector, t, 0)) affine-transform-file=$dir/configs/lda.mat

# the first splicing is moved before the lda layer, so no splicing here
relu-renorm-layer name=tdnn1 dim=1748
relu-renorm-layer name=tdnn2 dim=1748 input=Append(-1,2)
relu-renorm-layer name=tdnn3 dim=1748 input=Append(-3,3)
relu-renorm-layer name=tdnn4 dim=1748 input=Append(-3,3)
relu-renorm-layer name=tdnn5 dim=1748 input=Append(-3,3)
relu-renorm-layer name=tdnn6 dim=1748 input=Append(-3,3)
relu-renorm-layer name=tdnn7 dim=1748 input=Append(-7,2)
relu-renorm-layer name=tdnn8 dim=1748
output-layer name=output dim=$num_targets max-change=2.1
EOF
steps/nnet3/xconfig_to_configs.py --xconfig-file $dir/configs/network.xconfig --config-dir $dir/configs/

if [[ $(hostname -f) == *.clsp.jhu.edu ]] && [ ! -d $dir/egs/storage ]; then
    utils/create_split_dir.pl /export/b0{3,5,6}/$USER/kaldi-data/egs/ocsr-cbr-$(date +'%m_%d_%H_%M')/$dir/egs/storage $dir/egs/storage
fi

steps/nnet3/train_dnn.py --stage=$train_stage \
    --cmd="$decode_cmd" \
    --feat.online-ivector-dir ${train_ivector_dir} \
    --feat.cmvn-opts="--norm-means=false --norm-vars=false" \
    --trainer.num-epochs 4 \
    --trainer.optimization.num-jobs-final 1 \
    --trainer.optimization.initial-effective-lrate 0.2057 \
    --trainer.optimization.final-effective-lrate 0.02057 \
    --egs.dir "$common_egs_dir" \
    --egs.stage "$egs_stage" \
    --cleanup.remove-egs $remove_egs \
    --cleanup.preserve-model-interval 70 \
    --feat-dir=$train_data_dir \
    --ali-dir $ali_dir \
    --lang data/lang \
    --use-gpu=no \
    --dir=$dir || exit 1; #alt
echo

rm $dir/.error || true 2>/dev/null
(
    steps/nnet3/decode.sh --nj $nj --cmd "$decode_cmd" --acwt 1.4 --post-decode-acwt 14.0 --scoring-opts "--min-lmwt 7 " --num-threads $num_threads --online-ivector-dir exp/nnet3${nnet3_affix}/ivectors_test_hires ${graph_dir} data/test_hires ${dir}/decode_test || exit 1 #alt
) || touch $dir/.error &

wait
[ -f $dir/.error ] && echo "$0: there was a problem while decoding" && exit 1

exit 0;
