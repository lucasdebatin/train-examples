#!/bin/bash
nj=1
gmm=tri3b
train_set=train
nnet3_affix= 

. ./cmd.sh
. ./path.sh
. ./utils/parse_options.sh

echo
echo "===== MLP ====="
echo
local/nnet3/run_tdnn5.sh

echo
echo "===== RNN ====="
echo
local/nnet3/run_lstm5.sh

echo
echo "===== HMMM ====="
echo

rm -r exp/chain${nnet3_affix} data/lang_chain

gmm_dir=exp/$gmm
train_data_dir=data/${train_set}_sp_hires_comb
train_ivector_dir=exp/nnet3${nnet3_affix}/ivectors_${train_set}_sp_hires_comb
lores_train_data_dir=data/${train_set}_sp_comb
ali_dir=exp/${gmm}_ali_${train_set}_sp_comb
tree_dir=exp/chain${nnet3_affix}/tree_bi${tree_affix}
lat_dir=exp/chain${nnet3_affix}/${gmm}_${train_set}_sp_comb_lats

for f in $gmm_dir/final.mdl $train_data_dir/feats.scp $train_ivector_dir/ivector_online.scp \
    $lores_train_data_dir/feats.scp $ali_dir/ali.1.gz $gmm_dir/final.mdl; do
    [ ! -f $f ] && echo "$0: expected file $f to exist" && exit 1
done

echo "$0: creating lang directory with one state per phone."
# Create a version of the lang/ directory that has one state per phone in the
# topo file. [note, it really has two states.. the first one is only repeated
# once, the second one has zero or more repeats.]

if [ -d data/lang_chain ]; then
    if [ data/lang_chain/L.fst -nt data/lang/L.fst ]; then
        echo "$0: data/lang_chain already exists, not overwriting it; continuing"
    else
        echo "$0: data/lang_chain already exists and seems to be older than data/lang..."
        echo " ... not sure what to do.  Exiting."
        exit 1;
    fi
else
    cp -r data/lang data/lang_chain
    silphonelist=$(cat data/lang_chain/phones/silence.csl) || exit 1;
    nonsilphonelist=$(cat data/lang_chain/phones/nonsilence.csl) || exit 1;
    # Use our special topology... note that later on may have to tune this
    # topology.
    steps/nnet3/chain/gen_topo.py $nonsilphonelist $silphonelist >data/lang_chain/topo
fi

# Get the alignments as lattices (gives the chain training more freedom).
# use the same num-jobs as the alignments
steps/align_fmllr_lats.sh --nj $nj --cmd "$train_cmd" ${lores_train_data_dir} data/lang $gmm_dir $lat_dir
rm $lat_dir/fsts.*.gz # save space

# Build a tree using our new topology.  We know we have alignments for the
# speed-perturbed data (local/nnet3/run_ivector_common.sh made them), so use
# those.

if [ -f $tree_dir/final.mdl ]; then
    echo "$0: $tree_dir/final.mdl already exists, refusing to overwrite it."
    exit 1;
fi
steps/nnet3/chain/build_tree.sh --frame-subsampling-factor 5 \
    --context-opts "--context-width=6 --central-position=5" \
    --cmd "$train_cmd" 7200 ${lores_train_data_dir} data/lang_chain $ali_dir $tree_dir #alt
    
echo
echo "===== MLP-HMM ====="
echo
local/chain/run_tdnn5.sh

echo
echo "===== RNN-HMM ====="
echo
local/chain/run_lstm5.sh
    
echo
echo "===== run_nn.sh script is finished ====="
echo
# Getting results [see RESULTS file]
#local/generate_results_file.sh 2> /dev/null > RESULTS
