#!/bin/bash
. ./path.sh || exit 1
. ./cmd.sh || exit 1
. utils/parse_options.sh || exit 1

nj=3

echo
echo "===== monophone ====="
echo
steps/decode.sh --config conf/decode.config --nj $nj --cmd "$decode_cmd" exp/mono0a/graph data/test exp/mono0a/decode || exit 1;

echo
echo "===== tri1 (delta+delta-delta) ====="
echo
steps/decode.sh --config conf/decode.config --nj $nj --cmd "$decode_cmd" exp/tri1/graph data/test exp/tri1/decode || exit 1;

echo
echo "===== tri2a (delta + delta-delta) ====="
echo
steps/decode.sh --config conf/decode.config --nj $nj --cmd "$decode_cmd" exp/tri2a/graph data/test exp/tri2a/decode || exit 1;

echo
echo "===== tri2b (LDA+MLLT) ====="
echo
steps/decode.sh --config conf/decode.config --nj $nj --cmd "$decode_cmd" exp/tri2b/graph data/test exp/tri2b/decode || exit 1;

echo
echo "===== tri3b which is LDA + MLLT + SAT ====="
echo
steps/decode_fmllr.sh --cmd "$decode_cmd" --nj $nj exp/tri3b/graph data/test exp/tri3b/decode || exit 1;

echo
echo "===== MLP ====="
echo
steps/nnet3/decode.sh --nj $nj --cmd "$decode_cmd"  --num-threads 1 --online-ivector-dir exp/nnet3/ivectors_test_hires exp/tri3b/graph data/test_hires exp/nnet3/tdnn_sp/decode_test || exit 1
echo
echo "===== RNN ====="
echo
steps/nnet3/decode.sh --nj $nj --cmd "$decode_cmd"  --num-threads 1 --extra-left-context 50 --extra-right-context 50 --online-ivector-dir exp/nnet3/ivectors_test_hires exp/tri3b/graph data/test_hires exp/nnet3/lstm_sp/decode_test || exit 1
echo
echo "===== MLP+ HMM ====="
echo
steps/nnet3/decode.sh --num-threads 1 --nj $nj --cmd "$decode_cmd" --acwt 1.0 --post-decode-acwt 10.0 --online-ivector-dir exp/nnet3/ivectors_test_hires --scoring-opts "--min-lmwt 5 " exp/chain/tdnn_sp_bi/graph data/test_hires exp/chain/tdnn_sp_bi/decode_test || exit 1;
echo
echo "===== RNN + HMM ====="
echo
steps/nnet3/decode.sh --num-threads 1 --nj $nj --cmd "$decode_cmd" --acwt 1.0 --post-decode-acwt 10.0 --extra-left-context 50 --extra-right-context 0 --frames-per-chunk "150" --online-ivector-dir exp/nnet3/ivectors_test_hires --scoring-opts "--min-lmwt 5 " exp/chain/lstm_sp_bi/graph data/test_hires exp/chain/lstm_sp_bi/decode_test || exit 1;
echo
echo "===== run_test.sh script is finished ====="
echo
