#!/bin/bash

# steps/info/chain_dir_info.pl exp/chain/tdnn_sp_bi/
# exp/chain/tdnn_sp_bi/: num-iters=384 nj=2..12 num-params=7.0M dim=40+100->3557 combine=-0.08->-0.08 xent:train/valid[255,383,final]=(-0.954,-0.911,-0.911/-0.979,-0.953,-0.952) logprob:train/valid[255,383,final]=(-0.071,-0.064,-0.064/-0.084,-0.079,-0.079)

# local/chain/compare_wer_general.sh exp/nnet3/tdnn0_sp exp/chain/tdnn_sp_bi
# System               exp/nnet3/tdnn0_spexp/chain/tdnn_sp_bi
# WER on dev(tg)      11.57     10.00
# WER on test(tg)        9.89      8.58
# Final train prob     -0.79890.7538   -0.0642
# Final valid prob     -0.77280.7590   -0.0788
# Final train prob (xent)                 -0.9113
# Final valid prob (xent)                 -0.9525

## how you run this (note: this assumes that the run_tdnn.sh soft link points here;
## otherwise call it directly in its location).
# by default:
# local/chain/run_tdnn.sh

# note, that you should probably adjust parallelisation to your setup
# if you have already run the corresponding non-chain nnet3 system
# (local/nnet3/run_tdnn.sh), you may want to run with --stage 14.

# This script is like run_tdnn_1a.sh except it uses an xconfig-based mechanism
# to get the configuration.

set -e -o pipefail

# First the options that are passed through to run_ivector_common.sh
# (some of which are also used in this script directly).
nj=3
decode_nj=3
min_seg_len=0.31 #alt
xent_regularize=0.02 #alt
train_set=train
gmm=tri3b  # the gmm for the target data
num_threads_ubm=1
nnet3_affix=  # cleanup affix for nnet3 and chain dirs, e.g. _cleaned

# The rest are configs specific to this script.  Most of the parameters
# are just hardcoded at this level, in the commands below.
train_stage=-10
tree_affix=  # affix for tree directory, e.g. "a" or "b", in case we change the configuration.
tdnn_affix=  #affix for TDNN directory, e.g. "a" or "b", in case we change the configuration.
common_egs_dir=  # you can set this to use previously dumped egs.

# End configuration section.
echo "$0 $@"  # Print the command line for logging

. ./cmd.sh
. ./path.sh
. ./utils/parse_options.sh

#if ! cuda-compiled; then
#  cat <<EOF && exit 1
#This script is intended to be used with GPUs but you have not compiled Kaldi with CUDA
#If you want to use GPUs (and have them), go to src/, and configure and make on a machine
#where "nvcc" is installed.
#EOF
#fi

#local/nnet3/run_ivector_common.sh --stage $stage \
#                                  --nj $nj \
#                                  --min-seg-len $min_seg_len \
#                                  --train-set $train_set \
#                                  --gmm $gmm \
#                                  --num-threads-ubm $num_threads_ubm \
#                                  --nnet3-affix "$nnet3_affix"

gmm_dir=exp/$gmm
graph_dir=$gmm_dir/graph_tg
ali_dir=exp/${gmm}_ali_${train_set}_sp_comb
tree_dir=exp/chain${nnet3_affix}/tree_bi${tree_affix}
lat_dir=exp/chain${nnet3_affix}/${gmm}_${train_set}_sp_comb_lats
dir=exp/chain${nnet3_affix}/tdnn${tdnn_affix}_sp_bi
train_data_dir=data/${train_set}_sp_hires_comb
lores_train_data_dir=data/${train_set}_sp_comb
train_ivector_dir=exp/nnet3${nnet3_affix}/ivectors_${train_set}_sp_hires_comb

mkdir -p $dir
echo "$0: creating neural net configs using the xconfig parser";

num_targets=$(tree-info $tree_dir/tree |grep num-pdfs|awk '{print $2}')
learning_rate_factor=$(echo "print 0.5/$xent_regularize" | python)

rm -rf $dir/configs

mkdir -p $dir/configs
cat <<EOF > $dir/configs/network.xconfig #alt
input dim=100 name=ivector
input dim=40 name=input

# please note that it is important to have input layer with the name=input
# as the layer immediately preceding the fixed-affine-layer to enable
# the use of short notation for the descriptor
fixed-affine-layer name=lda input=Append(-1,0,1,ReplaceIndex(ivector, t, 0)) affine-transform-file=$dir/configs/lda.mat

# the first splicing is moved before the lda layer, so no splicing here
relu-renorm-layer name=tdnn1 dim=90
relu-renorm-layer name=tdnn2 input=Append(-1,0,1) dim=90

## adding the layers for chain branch
relu-renorm-layer name=prefinal-chain input=tdnn2 dim=90 target-rms=0.1
output-layer name=output include-log-softmax=false dim=$num_targets max-change=0.3

# adding the layers for xent branch
# This block prints the configs for a separate output that will be
# trained with a cross-entropy objective in the 'chain' models... this
# has the effect of regularizing the hidden parts of the model.  we use
# 0.5 / args.xent_regularize as the learning rate factor- the factor of
# 0.5 / args.xent_regularize is suitable as it means the xent
# final-layer learns at a rate independent of the regularization
# constant; and the 0.5 was tuned so as to make the relative progress
# similar in the xent and regular final layers.
relu-renorm-layer name=prefinal-xent input=tdnn2 dim=90 target-rms=0.1
output-layer name=output-xent dim=$num_targets learning-rate-factor=$learning_rate_factor max-change=0.3

EOF
steps/nnet3/xconfig_to_configs.py --xconfig-file $dir/configs/network.xconfig --config-dir $dir/configs/

if [[ $(hostname -f) == *.clsp.jhu.edu ]] && [ ! -d $dir/egs/storage ]; then
    utils/create_split_dir.pl \
        /export/b0{5,6,7,8}/$USER/kaldi-data/egs/ami-$(date +'%m_%d_%H_%M')/s5/$dir/egs/storage $dir/egs/storage
fi

steps/nnet3/chain/train.py --stage $train_stage \
    --cmd "$decode_cmd" \
    --feat.online-ivector-dir $train_ivector_dir \
    --feat.cmvn-opts "--norm-means=false --norm-vars=false" \
    --chain.xent-regularize $xent_regularize \
    --chain.leaky-hmm-coefficient 0.02 \
    --chain.l2-regularize 0.00005 \
    --chain.apply-deriv-weights false \
    --chain.lm-opts="--num-extra-lm-states=400" \
    --egs.dir "$common_egs_dir" \
    --egs.opts "--frames-overlap-per-eg 0" \
    --egs.chunk-width 30 \
    --trainer.num-chunk-per-minibatch 32 \
    --trainer.frames-per-iter 300000 \
    --trainer.num-epochs 1 \
    --trainer.optimization.num-jobs-final 1 \
    --trainer.optimization.initial-effective-lrate 0.001 \
    --trainer.optimization.final-effective-lrate 0.0001 \
    --trainer.max-param-change 0.4 \
    --cleanup.remove-egs true \
    --feat-dir $train_data_dir \
    --tree-dir $tree_dir \
    --lat-dir $lat_dir \
    --dir $dir #alt

# Note: it might appear that this data/lang_chain directory is mismatched, and it is as
# far as the 'topo' is concerned, but this script doesn't read the 'topo' from
# the lang directory.
utils/mkgraph.sh --self-loop-scale 0.2 data/lang $dir $dir/graph #alt

rm $dir/.error 2>/dev/null || true
for dset in test; do
    (
    steps/nnet3/decode.sh --num-threads 1 --nj $decode_nj --cmd "$decode_cmd" \
        --acwt 0.2 --post-decode-acwt 2.0 \
        --online-ivector-dir exp/nnet3${nnet3_affix}/ivectors_${dset}_hires \
        --scoring-opts "--min-lmwt 1 " \
        $dir/graph data/${dset}_hires $dir/decode_${dset} || exit 1; #alt
) || touch $dir/.error &
done
wait
if [ -f $dir/.error ]; then
    echo "$0: something went wrong in decoding"
    exit 1
fi
exit 0
