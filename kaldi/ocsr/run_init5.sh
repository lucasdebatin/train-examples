#!/bin/bash
. ./path.sh || exit 1
. ./cmd.sh || exit 1
nj=3       # number of parallel jobs - 1 is perfect for such a small data set
lm_order=4 # language model order (n-gram quantity) - 1 is enough for digits grammar] #alt
# Safety mechanism (possible running this script with modified arguments)
. utils/parse_options.sh || exit 1
[[ $# -ge 1 ]] && { echo "Wrong arguments!"; exit 1; }
# Removing previously created data (from last run.sh execution)
rm -rf exp mfcc data/train/spk2utt data/train/cmvn.scp data/train/feats.scp data/train/split1 data/test/spk2utt data/test/cmvn.scp data/test/feats.scp data/test/split1 data/local/lang data/lang data/local/tmp data/local/dict/lexiconp.txt data/test_hires data/train_sp data/train_sp_hires data/train_sp_hires_comb exp/nnet3/tri5 exp/nnet3/diag_ubm exp/tri3b_ali_train_sp_comb

echo
echo "===== PREPARING ACOUSTIC DATA ====="
echo
# Needs to be prepared by hand (or using self written scripts):
#
# spk2gender  [<speaker-id> <gender>]
# wav.scp     [<uterranceID> <full_path_to_audio_file>]
# text        [<uterranceID> <text_transcription>]
# utt2spk     [<uterranceID> <speakerID>]
# corpus.txt  [<text_transcription>]
# Making spk2utt files
utils/utt2spk_to_spk2utt.pl data/train/utt2spk > data/train/spk2utt
utils/utt2spk_to_spk2utt.pl data/test/utt2spk > data/test/spk2utt
echo
echo "===== FEATURES EXTRACTION ====="
echo
# Making feats.scp files
mfccdir=mfcc
# Uncomment and modify arguments in scripts below if you have any problems with data sorting
# utils/validate_data_dir.sh data/train     # script for checking prepared data - here: for data/train directory
# utils/fix_data_dir.sh data/train          # tool for data proper sorting if needed - here: for data/train directory
steps/make_mfcc.sh --nj $nj --cmd "$train_cmd" data/train exp/make_mfcc/train $mfccdir
steps/make_mfcc.sh --nj $nj --cmd "$train_cmd" data/test exp/make_mfcc/test $mfccdir
# Making cmvn.scp files
steps/compute_cmvn_stats.sh data/train exp/make_mfcc/train $mfccdir
steps/compute_cmvn_stats.sh data/test exp/make_mfcc/test $mfccdir
# Repair data set (remove corrupt data points with corrupt audio)
utils/fix_data_dir.sh data/train || exit 1;
utils/fix_data_dir.sh data/test || exit 1;
echo
echo "===== PREPARING LANGUAGE DATA ====="
echo
# Needs to be prepared by hand (or using self written scripts):
#
# lexicon.txt           [<word> <phone 1> <phone 2> ...]
# nonsilence_phones.txt [<phone>]
# silence_phones.txt    [<phone>]
# optional_silence.txt  [<phone>]
# Preparing language data
utils/prepare_lang.sh data/local/dict "<UNK>" data/local/lang data/lang
echo
echo "===== LANGUAGE MODEL CREATION ====="
echo "===== MAKING lm.arpa ====="
echo
loc=`which ngram-count`;
if [ -z $loc ]; then
    if uname -a | grep 64 >/dev/null; then
        sdir=$KALDI_ROOT/tools/srilm/bin/i686-m64
    else
        sdir=$KALDI_ROOT/tools/srilm/bin/i686
    fi
    if [ -f $sdir/ngram-count ]; then
        echo "Using SRILM language modelling tool from $sdir"
        export PATH=$PATH:$sdir
    else
        echo "SRILM toolkit is probably not installed.
        Instructions: tools/install_srilm.sh"
        exit 1
    fi
fi
local=data/local
mkdir $local/tmp
ngram-count -order $lm_order -write-vocab $local/tmp/vocab-full.txt -wbdiscount -text $local/corpus.txt -lm $local/tmp/lm.arpa
echo
echo "===== MAKING G.fst ====="
echo
lang=data/lang
arpa2fst --disambig-symbol=#0 --read-symbol-table=$lang/words.txt $local/tmp/lm.arpa $lang/G.fst
echo
echo "===== TRAINING ====="
echo
# Train monophone model on short utterances
steps/train_mono.sh --nj $nj --cmd "$train_cmd" data/train data/lang exp/mono0a || exit 1;
utils/mkgraph.sh --mono data/lang exp/mono0a exp/mono0a/graph || exit 1;
steps/decode.sh --config conf/decode.config --nj $nj --cmd "$decode_cmd" exp/mono0a/graph data/test exp/mono0a/decode || exit 1;

# Train tri1 (delta+delta-delta)
steps/align_si.sh --nj $nj --cmd "$train_cmd" data/train data/lang exp/mono0a exp/mono0a_ali || exit 1;
steps/train_deltas.sh --cmd "$train_cmd" 5400 72000 data/train data/lang exp/mono0a_ali exp/tri1 || exit 1; #alt

# Decode dev set with both LMs
utils/mkgraph.sh data/lang exp/tri1 exp/tri1/graph || exit 1;
steps/decode.sh --config conf/decode.config --nj $nj --cmd "$decode_cmd" exp/tri1/graph data/test exp/tri1/decode || exit 1;

# Train tri2a (delta + delta-delta)
steps/align_si.sh --nj $nj --cmd "$train_cmd" data/train data/lang exp/tri1 exp/tri1_ali || exit 1;
steps/train_deltas.sh --cmd "$train_cmd" 9000 108000 data/train data/lang exp/tri1_ali exp/tri2a || exit 1; #alt
utils/mkgraph.sh data/lang exp/tri2a exp/tri2a/graph || exit 1;
steps/decode.sh --config conf/decode.config --nj $nj --cmd "$decode_cmd" exp/tri2a/graph data/test exp/tri2a/decode || exit 1;

# Train tri2b (LDA+MLLT)
steps/align_si.sh --nj $nj --cmd "$train_cmd" data/train data/lang exp/tri2a exp/tri2a_ali || exit 1;
steps/train_lda_mllt.sh --cmd "$train_cmd" --splice-opts "--left-context=9 --right-context=9" 11700 135000 data/train data/lang exp/tri2a_ali exp/tri2b || exit 1; #alt
utils/mkgraph.sh data/lang exp/tri2b exp/tri2b/graph || exit 1;
steps/decode.sh --config conf/decode.config --nj $nj --cmd "$decode_cmd" exp/tri2b/graph data/test exp/tri2b/decode || exit 1;

# From 2b system, train 3b which is LDA + MLLT + SAT.
steps/align_si.sh  --nj $nj --cmd "$train_cmd" --use-graphs true data/train data/lang exp/tri2b exp/tri2b_ali  || exit 1;
steps/train_sat.sh --cmd "$train_cmd" 13500 180000 data/train data/lang exp/tri2b_ali exp/tri3b || exit 1; #alt

# Decode dev with 4gram and 3gram LMs
utils/mkgraph.sh data/lang exp/tri3b exp/tri3b/graph || exit 1;
steps/decode_fmllr.sh --cmd "$decode_cmd" --nj $nj exp/tri3b/graph data/test exp/tri3b/decode || exit 1;

# Decode test with 4gram and 3gram LMs
# there are fewer speaker (n=7) and decoding usually ends up waiting
# for a single job so we use --num-threads 2 to speed up
steps/decode_fmllr.sh --cmd "$decode_cmd" --nj $nj --num-threads 5 exp/tri3b/graph data/test exp/tri3b/decode || exit 1; #alt

# Alignment used to train nnets and sgmms
steps/align_fmllr.sh --nj $nj --cmd "$train_cmd" data/train data/lang exp/tri3b exp/tri3b_ali || exit 1;
echo
echo "===== Ivector Common NNET3 ====="
echo
min_seg_len=2.79 #alt
train_set=train
gmm=tri3b  # this is the source gmm-dir for the data-type of interest; it
           # should have alignments for the specified training data.
num_threads_ubm=1
nnet3_affix=  # cleanup affix for exp dirs, e.g. _cleaned
local/nnet3/run_ivector_common5.sh --nj $nj \
                                    --min-seg-len $min_seg_len \
                                    --train-set $train_set \
                                    --gmm $gmm \
                                    --num-threads-ubm $num_threads_ubm \
                                    --nnet3-affix "$nnet3_affix"
echo
echo "===== run_init.sh script is finished ====="
echo
