for num in 1 2 3 4 5
do 
    echo "Start OCSR --> $num"
    cd /home/debatin/Documents/Toolkits/htk/ocsr/
    rm -r *G hmms* lattices hviteout.txt lattices.list phones* stats tiedlist tree.hed trifone trigram.out wdnet wintri.mlf output/dateIni$num.txt output/OutputFrontEnd$num output/OutputAM01$num output/OutputAM02$num output/OutputAM03$num output/OutputAM04$num output/OutputTest$num output/dateFin$num.txt
    mv AM_01.sh$num AM_01.sh
    mv AM_02.sh$num AM_02.sh
    mv AM_03.sh$num AM_03.sh
    mv AM_04.sh$num AM_04.sh
    mv Test.sh$num Test.sh
    mv confs/hcopy-wav.conf6 confs/hcopy-wav.conf
    mv confs/hvite.conf6 confs/hvite.conf
    echo `date` > output/dateIni$num.txt
    ./FrontEnd.sh > output/OutputFrontEnd$num
    ./AM_01.sh > output/OutputAM01$num
    ./AM_02.sh > output/OutputAM02$num
    ./AM_03.sh > output/OutputAM03$num
    ./AM_04.sh > output/OutputAM04$num
    ./Test.sh > output/OutputTest$num
    echo `date` > output/dateFin$num.txt
    mv AM_01.sh AM_01.sh$num
    mv AM_02.sh AM_02.sh$num
    mv AM_03.sh AM_03.sh$num
    mv AM_04.sh AM_04.sh$num
    mv Test.sh Test.sh$num
    mv confs/hcopy-wav.conf confs/hcopy-wav.conf6
    mv confs/hvite.conf confs/hvite.conf6
    echo "Finish OCSR --> $num"
done

for num in 1 2 3 4 5
do 
    echo "Start OCSR-CBR --> $num"
    cd /home/debatin/Documents/Toolkits/htk/ocsr-cbr/
    rm -r *G hmms* lattices hviteout.txt lattices.list phones* stats tiedlist tree.hed trifone trigram.out wdnet wintri.mlf output/dateIni$num.txt output/OutputFrontEnd$num output/OutputAM01$num output/OutputAM02$num output/OutputAM03$num output/OutputAM04$num output/OutputTest$num output/dateFin$num.txt
    mv AM_01.sh$num AM_01.sh
    mv AM_02.sh$num AM_02.sh
    mv AM_03.sh$num AM_03.sh
    mv AM_04.sh$num AM_04.sh
    mv Test.sh$num Test.sh
    mv confs/hcopy-wav.conf6 confs/hcopy-wav.conf
    mv confs/hvite.conf6 confs/hvite.conf
    echo `date` > output/dateIni$num.txt
    ./FrontEnd.sh > output/OutputFrontEnd$num
    ./AM_01.sh > output/OutputAM01$num
    ./AM_02.sh > output/OutputAM02$num
    ./AM_03.sh > output/OutputAM03$num
    ./AM_04.sh > output/OutputAM04$num
    ./Test.sh > output/OutputTest$num
    echo `date` > output/dateFin$num.txt
    mv AM_01.sh AM_01.sh$num
    mv AM_02.sh AM_02.sh$num
    mv AM_03.sh AM_03.sh$num
    mv AM_04.sh AM_04.sh$num
    mv Test.sh Test.sh$num
    mv confs/hcopy-wav.conf confs/hcopy-wav.conf6
    mv confs/hvite.conf confs/hvite.conf6
    echo "Finish OCSR-CBR --> $num"
done

for num in 1 2 3 4 5
do 
    echo "Start OCSR --> $num"
    cd /home/debatin/Documents/Toolkits/htk/ocsr/
    rm -r *G hmms* lattices hviteout.txt lattices.list phones* stats tiedlist tree.hed trifone trigram.out wdnet wintri.mlf output/dateIni$num.txt output/OutputFrontEnd$num output/OutputAM01$num output/OutputAM02$num output/OutputAM03$num output/OutputAM04$num output/OutputTest$num output/dateFin$num.txt
    mv AM_01.sh$num AM_01.sh
    mv AM_02.sh$num AM_02.sh
    mv AM_03.sh$num AM_03.sh
    mv AM_04.sh$num AM_04.sh
    mv Test.sh$num Test.sh
    mv confs/hcopy-wav.conf$num confs/hcopy-wav.conf
    mv confs/hvite.conf$num confs/hvite.conf
    echo `date` > output/dateIni$num.txt
    ./FrontEnd.sh > output/OutputFrontEnd$num
    ./AM_01.sh > output/OutputAM01$num
    ./AM_02.sh > output/OutputAM02$num
    ./AM_03.sh > output/OutputAM03$num
    ./AM_04.sh > output/OutputAM04$num
    ./Test.sh > output/OutputTest$num
    echo `date` > output/dateFin$num.txt
    mv AM_01.sh AM_01.sh$num
    mv AM_02.sh AM_02.sh$num
    mv AM_03.sh AM_03.sh$num
    mv AM_04.sh AM_04.sh$num
    mv Test.sh Test.sh$num
    mv confs/hcopy-wav.conf confs/hcopy-wav.conf$num
    mv confs/hvite.conf confs/hvite.conf$num
    echo "Finish OCSR --> $num"
done

for num in 1 2 3 4 5
do 
    echo "Start OCSR-CBR --> $num"
    cd /home/debatin/Documents/Toolkits/htk/ocsr-cbr/
    rm -r *G hmms* lattices hviteout.txt lattices.list phones* stats tiedlist tree.hed trifone trigram.out wdnet wintri.mlf output/dateIni$num.txt output/OutputFrontEnd$num output/OutputAM01$num output/OutputAM02$num output/OutputAM03$num output/OutputAM04$num output/OutputTest$num output/dateFin$num.txt
    mv AM_01.sh$num AM_01.sh
    mv AM_02.sh$num AM_02.sh
    mv AM_03.sh$num AM_03.sh
    mv AM_04.sh$num AM_04.sh
    mv Test.sh$num Test.sh
    mv confs/hcopy-wav.conf$num confs/hcopy-wav.conf
    mv confs/hvite.conf$num confs/hvite.conf
    echo `date` > output/dateIni$num.txt
    ./FrontEnd.sh > output/OutputFrontEnd$num
    ./AM_01.sh > output/OutputAM01$num
    ./AM_02.sh > output/OutputAM02$num
    ./AM_03.sh > output/OutputAM03$num
    ./AM_04.sh > output/OutputAM04$num
    ./Test.sh > output/OutputTest$num
    echo `date` > output/dateFin$num.txt
    mv AM_01.sh AM_01.sh$num
    mv AM_02.sh AM_02.sh$num
    mv AM_03.sh AM_03.sh$num
    mv AM_04.sh AM_04.sh$num
    mv Test.sh Test.sh$num
    mv confs/hcopy-wav.conf confs/hcopy-wav.conf$num
    mv confs/hvite.conf confs/hvite.conf$num
    echo "Finish OCSR-CBR --> $num"
done
echo "Done!"
