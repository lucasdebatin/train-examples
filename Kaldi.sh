for num in 1 2 3 4 5
do 
    echo "Start OCSR --> $num"
    cd /home/debatin/Documents/Toolkits/kaldi-trunk/egs/ocsr/
    rm -r dateIni$num.txt OutputRunInit$num dateFin$num.txt data/train_sp_hires_comb
    mv run_init$num.sh run_init.sh
    echo `date` > dateIni$num.txt
    ./run_init.sh > OutputRunInit$num
    echo `date` > dateFin$num.txt
    mv run_init.sh run_init$num.sh
    echo "Finish OCSR --> $num"
done

for num in 1 2 3 4 5
do 
    echo "Start OCSR-CBR --> $num"
    cd /home/debatin/Documents/Toolkits/kaldi-trunk/egs/ocsr-cbr/
    rm -r dateIni$num.txt OutputRunInit$num dateFin$num.txt data/train_sp_hires_comb
    mv run_init$num.sh run_init.sh
    echo `date` > dateIni$num.txt
    ./run_init.sh > OutputRunInit$num
    echo `date` > dateFin$num.txt
    mv run_init.sh run_init$num.sh
    echo "Finish OCSR-CBR --> $num"
done

for num in 1 2 3 4 5
do 
    echo "Start OCSR --> $num"
    cd /home/debatin/Documents/Toolkits/kaldi-trunk/egs/ocsr/
    rm -r dateIni$num.txt OutputRunInit$num OutputRunNN$num dateFin$num.txt data/train_sp_hires_comb
    mv run_init$num.sh run_init.sh
    mv run_nn$num.sh run_nn.sh
    echo `date` > dateIni$num.txt
    ./run_init.sh > OutputRunInit$num
    ./run_nn.sh > OutputRunNN$num
    echo `date` > dateFin$num.txt
    mv run_init.sh run_init$num.sh
    mv run_nn.sh run_nn$num.sh
    echo "Finish OCSR --> $num"
done

for num in 1 2 3 4 5
do 
    echo "Start OCSR-CBR --> $num"
    cd /home/debatin/Documents/Toolkits/kaldi-trunk/egs/ocsr-cbr/
    rm -r dateIni$num.txt OutputRunInit$num OutputRunNN$num dateFin$num.txt data/train_sp_hires_comb
    mv run_init$num.sh run_init.sh
    mv run_nn$num.sh run_nn.sh
    echo `date` > dateIni$num.txt
    ./run_init.sh > OutputRunInit$num
    ./run_nn.sh > OutputRunNN$num
    echo `date` > dateFin$num.txt
    mv run_init.sh run_init$num.sh
    mv run_nn.sh run_nn$num.sh
    echo "Finish OCSR-CBR --> $num"
done
echo "Done!"
