for num in 1 2 3 4 5 6 7 8 9 10
do 
    echo "Start OCSR --> $num"
    cd /home/debatin/Documents/Toolkits/cmu-sphinx/ocsr/etc/
    mv sphinx_train$num.cfg sphinx_train.cfg
    cd ../
    rm -r dateIni$num.txt TestPerformance$num.txt TestOutput$num dateFin$num.txt bwaccumdir/* denlat/* feat/* g2p/* logdir/* model_architecture/* model_parameters/* numlat/* qmanager/* result/* trees/*
    echo `date` > dateIni$num.txt
    sphinxtrain run > TestOutput$num
    echo `date` > dateFin$num.txt
    cd etc/
    mv sphinx_train.cfg sphinx_train$num.cfg
    echo "Finish OCSR --> $num"
done

for num in 1 2 3 4 5 6 7 8 9 10
do 
    echo "Start OCSR-CBR --> $num"
    cd /home/debatin/Documents/Toolkits/cmu-sphinx/ocsr-cbr/etc/
    mv sphinx_train$num.cfg sphinx_train.cfg
    cd ../
    rm -r dateIni$num.txt TestPerformance$num.txt TestOutput$num dateFin$num.txt bwaccumdir/* denlat/* feat/* g2p/* logdir/* model_architecture/* model_parameters/* numlat/* qmanager/* result/* trees/*
    echo `date` > dateIni$num.txt
    sphinxtrain run > TestOutput$num
    echo `date` > dateFin$num.txt
    cd etc/
    mv sphinx_train.cfg sphinx_train$num.cfg
    echo "Finish OCSR-CBR --> $num"
done
echo "Done!"
